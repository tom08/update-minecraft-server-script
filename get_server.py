import json
from subprocess import call

VERSION_MANIFEST_WEB="https://launchermeta.mojang.com/mc/game/version_manifest.json"

call(["rm", "version_manifest.json"])
call(["wget", VERSION_MANIFEST_WEB,])

version_string = ""
version_json_url = ""

def main():
    with open("version_manifest.json", "r") as f:
        manifest = json.load(f)
        version_string = manifest.get("latest", {}).get("release", None)

        if not version_string:
            print("Unable to get version!")
            return
        
        print("Getting json for version: "+version_string)
        versions = manifest.get("versions", None)
        if not versions:
            print("Unable to get versions!")
            return
        for version in versions:
            if version.get("id") == version_string:
                version_json_url = version.get("url", None)
                break
        if not version_json_url:
            print("version url got: "+version_json_url)
            return

    call(["wget", version_json_url,])

    with open(version_string+".json", "r") as f:
        version_json = json.load(f)
        server_url = version_json.get("downloads", {}).get("server", {}).get("url", None)
        if not server_url:
            print("Unable to extract the server .jar url.")
            return
        call(["wget", server_url,])
        call(["rm", "last_server_version.jar",])
        call(["mv", "minecraft_server.jar", "last_server_version.jar"])
        call(["mv", "server.jar", "minecraft_server.jar",])

main()
